package br.ucsal;

import java.util.Scanner;

public class Inteiros {

	public static void main(String[] args) {
		
		Scanner in = new Scanner(System.in);
		
		int [] array = new int [10];
		int media = 0;
		int maior = Integer.MIN_VALUE;
		int menor = Integer.MAX_VALUE;
		
		for(int i = 0; i < array.length; i++) {
			
			array[i] = in.nextInt();
			
			media += array[i];
			
			
			if(array[i] > maior) {
				maior = array[i];
			}
			
			if(array[i] <= menor) {
				menor = array[i];
			}
			
			
			
		}
		
		media = media/10;
		
		System.out.println(media);
		
		System.out.println(maior);
		
		System.out.println(menor);
		
		in.close();

	}

}
