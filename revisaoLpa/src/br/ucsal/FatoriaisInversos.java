package br.ucsal;

import java.util.Scanner;

public class FatoriaisInversos {

	public static void main(String[] args) {
		int n;
		double result;
		n = getN();
		getFactorial(n);
		result = getInverserFactorialSum(n);

		System.out.println(result);
	}

	private static double getInverserFactorialSum(int n) {
		
		return n == 0 ? 1 : 1/getFactorial(n) + getInverserFactorialSum(n-1);
		
	}

	private static double getFactorial(int n) {

		return n == 0 ? 1 : n * getFactorial(n - 1);
	}

	private static int getN() {
		int n;

		Scanner in = new Scanner(System.in);

		n = in.nextInt();

		in.close();

		return n;
	}

}
