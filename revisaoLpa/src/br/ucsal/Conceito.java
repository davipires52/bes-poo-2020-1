package br.ucsal;

import java.util.Scanner;

public class Conceito {

	public static void main(String[] args) {
		general();
	}

	private static void general() {
		String conceito = null;
		int nota = 0;  
		
		nota = getGrades();
		conceito = calculateGrade(nota);
	
		print("O conceito para a nota " + nota +" " + "é " + conceito);
	}

	private static void print(String txt) {
		
		System.out.println(txt);
		
	}

	private static String calculateGrade(int nota) {
		String conceito = null;
		
		if (nota <= 49) {
			conceito = ("Insuficiente");
		} else if (nota <= 64) {
			conceito = ("Regular");
		} else if (nota <= 84) {
			conceito = ("Bom");
		} else {

			conceito = ("Ótimo");

		}
		
		return conceito;
	}

	private static int getGrades() {
		
		int nota;
		
		Scanner input = new Scanner(System.in);

		nota = input.nextInt();
		
		input.close();
		
		return nota;
		
	}

}
