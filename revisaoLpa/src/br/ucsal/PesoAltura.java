package br.ucsal;

import java.util.Scanner;

public class PesoAltura {

	public static void main(String[] args) {

		String[] names = new String[3];
		Double[] heights = new Double[names.length];
		Double[] weights = new Double[names.length];
		double maxHeight;
		double maxWeight;
		int maxPositionHeight;
		int maxPositionWeight;

		names = getNames(names);
		heights = getHeights(heights);
		weights = getWeights(weights);

		maxHeight = getMaxHeights(heights);
		maxWeight = getMaxWeights(weights);

		maxPositionHeight = getPositionHeight(heights,maxHeight );
		maxPositionWeight = getPositionWeight(weights, maxWeight);

		System.out.println("Mais alta : " + names[maxPositionHeight]+ " " + maxHeight  + "\n" + "Mais pesada: " + names[maxPositionWeight] + " " + maxWeight);

	}

	private static int getPositionWeight(Double[] weights, double maxWeight) {
		int position = 0;

		for (int i = 0; i < weights.length; i++) {
			if (weights[i] == maxWeight) {
				position = i;
				
			}
		}

		return position;
	}

	private static int getPositionHeight(Double[] heights, double maxHeight) {
		int position = 0;

		for (int i = 0; i < heights.length; i++) {
			if (heights[i] == maxHeight) {
				position = i;
				
			}
		}

		return position;
		
	}

	private static double getMaxWeights(Double[] weights) {
		double maxWeight = Double.MIN_VALUE;

		for (int i = 0; i < weights.length; i++) {
			if (weights[i] > maxWeight) {
				maxWeight = weights[i];
			}
		}

		return maxWeight;
	}

	private static double getMaxHeights(Double[] heights) {
		double maxHeight = Double.MIN_VALUE;

		for (int i = 0; i < heights.length; i++) {
			if (heights[i] > maxHeight) {
				maxHeight = heights[i];
			}
		}

		return maxHeight;
	}

	private static Double[] getHeights(Double[] heights0) {
		Scanner in = new Scanner(System.in);
		Double[] heights = new Double[heights0.length];

		System.out.println("Digite sua altura: ");

		for (int i = 0; i < heights0.length; i++) {
			heights[i] = in.nextDouble();
		}

		return heights;
	}

	private static Double[] getWeights(Double[] weights0) {
		Scanner in = new Scanner(System.in);
		Double[] weights = new Double[weights0.length];

		System.out.println("Digite seu peso: ");

		for (int i = 0; i < weights0.length; i++) {
			weights[i] = in.nextDouble();
		}

		return weights;
	}

	private static String[] getNames(String[] names0) {
		Scanner in = new Scanner(System.in);
		String[] names = new String[names0.length];

		System.out.println("Digite seu nome: ");

		for (int i = 0; i < names0.length; i++) {
			names[i] = in.next();
		}

		return names;
	}

}
