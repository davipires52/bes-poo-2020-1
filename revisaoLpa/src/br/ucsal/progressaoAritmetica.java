package br.ucsal;

import java.util.Scanner;

public class progressaoAritmetica {

	public static void main(String[] args) {

		int n;
		int a1;
		int r;
		int[] numbersArray;
		int finalNumbers[] = null;
		int sum;

		numbersArray = getNumbers();

		n = numbersArray[0];
		a1 = numbersArray[1];
		r = numbersArray[2];

		finalNumbers = getFinalNumbers(n, a1, r);

		sum = getSum(finalNumbers);

		showNumbers(finalNumbers);
		
		showSum(sum);

	}


	private static void showNumbers(int[] finalNumbers) {
		System.out.println("Lista de numeros da Pa: ");

		for (int i = 0; i < finalNumbers.length; i++) {
			System.out.println(finalNumbers[i]);
		}

	}
	
	private static void showSum(int sum) {
		
		System.out.println("Soma de todos os números: " + "\n" + sum);
		
	}

	private static int getSum(int[] finalNumbers) {
		int sum = 0;
		
		for (int i = 0; i < finalNumbers.length; i++) {

			sum += finalNumbers[i];

		}
		return sum;

	}

	private static int[] getFinalNumbers(int n, int a1, int r) {

		int[] finalNumbers = new int[n];

		for (int i = 0; i < n; i++) {

			finalNumbers[i] = a1;

			a1 += r;
		}

		return finalNumbers;
	}

	private static int[] getNumbers() {
		int[] numbers = new int[3];

		Scanner in = new Scanner(System.in);

		System.out.println("Informe o valor de n, a1 e r: ");

		for (int i = 0; i < numbers.length; i++) {
			numbers[i] = in.nextInt();
		}
		
		in.close();

		return numbers;

	}

}
