package lista1Poo;

import java.util.Scanner;

public class MaiorNumero {

	public static void main(String[] args) {
		int [] vet = new int [5];
		int maior;
		
		obterNumeros(vet);
		maior = encontrarMaiorNumero (vet);
		exibirMaiorNumero(maior);
		
	}

	public static void obterNumeros(int[] vet) {
		
		Scanner in = new Scanner(System.in);
		
		System.out.println("Informe 5 valores: ");
		
		for(int i = 0; i < vet.length; i++) {
			vet[i] = in.nextInt();
		}
		
	}

	public static int encontrarMaiorNumero(int[] vet) {
		
		int maxNumber = 0;
		
		for(int i = 0; i < vet.length; i++) {
			  
			if(maxNumber < vet[i]) {
				maxNumber = vet[i];
			}
		
		}
		
		return maxNumber;
	}

	public static void exibirMaiorNumero(int maior) {

		System.out.println(maior);
		
	}

}
