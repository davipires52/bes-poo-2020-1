package lista1Poo;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class testEncontrarMaiorNumero {

	@Test
	void testEncontarMaiorNumero() {
		int [] vet = {4, 8, 7, 4 , 3};
		
		MaiorNumero jUnit = new MaiorNumero();
		
		int resultado = jUnit.encontrarMaiorNumero(vet);
		
		assertEquals(8, resultado);
		
		
	}

}
